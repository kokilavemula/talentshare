var mongoose = require('mongoose');
var connection = null;
console.log('data in mongo db : ');
module.exports.connect=function(options){
    console.log('data in mongo db : inside if conn ');
    if(mongoose.connection.readyState == 0 && connection == null){
        connection = mongoose.connect(options,(err)=>{
            if(err){
                console.log('err : ',err);
            }
        })
        mongoose.connection.on('error',function(err){
            if(err){
                connection = null;
                console.log('db err : ',err);
            }
        })
        mongoose.connection.on('open',function(ref){
            console.log(' acl db connected');
        })
    }
    return connection;
}

