const projectAuditDetails = require('../model/audit');
const atob = require('atob');
const config = require('config');
    const userDetails = (req,res)=>{
      const data = req.headers.authorization;
      const splitedData = data.split('.');
      const userData =JSON.parse(atob(splitedData[1]));
      const auditDetails = {
        actor: {
          objectType : 'person',
          email : userData.email,
          id : userData.id
      },
      object : {
          objectType : 'audit'
      },
      browserDetails : {
          name : req.headers['user-agent'],
          ipAddress : req.connection.remoteAddress
      },
      verb : 'Auditing',
      message : '',
      action : '',
      }
      return auditDetails
    }
    const createAuditDetails = (req, res)=>{
    return projectAuditDetails.create(req).then(data=>{
      console.log('audit Details Created successfully');
    }).catch(err=>{
      console.log('error in storing audit details');
    })
        
}
exports.userDetails = userDetails;
exports.createAuditDetails = createAuditDetails;