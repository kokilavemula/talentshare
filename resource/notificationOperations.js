var notification = require('../model/notification');
var config = require('config');
function addUserIntoSocket(socket){
  if(socket!= null){
      user.find().then(data=>{
          for(let i=0;i<data.length;i++){
              let id = data[i]._id;
              if(id !== null){
                  connectedUser[id]=socket;
              }
          }
      })
      .catch(err=>{
          logger.error('Error while adding user id into socket');
          return err;
      })
  } else{
      return;
  }
}
const createNotifications = (req, res)=>{
    let userData = req.body;
    return notification.create(userData,function(error,data){
        if(error){
          return res.send({
            status : 400,
            message : 'bad request'
          });
        }
        else{
            console.log('created notification',data)
          return res.send({
            status : 200,
            message : data
          })
        }
    })
}
const getAllNotification = (req, res)=>{
    return notification.find(function(error,data){
        if(error){
          return res.send({
            status : 400,
            message : 'bad request'
          });
        }
        else{
            console.log('getnotificationdata is',data);
          return res.send({
            status : 200,
            message : data
          })
        }
    })
}
const notificationByEmail = (req, res)=>{
  var email = req.params.email;
  console.log('email is',email);
 return notification.findOne({email}, function (err, users){
   // console.log(users._id);
   if(err){
     console.log(err);
   }
   else {
   console.log('notification data:',users);
  return res.json(users);
   
   }
 }); 
};
var updateNotificationDetails = function(req,res){
  return notification.findByIdAndUpdate(req.params.id,req.body,function(error,data){
      if(!error){
          console.log('data from put method : ',data);
          res.send({
              statusCode : 200,
              message : data
          });
      }
      else{
          // console.log('error : ',error);
          res.send({
              statusCode : 500,
              error : error
          })
      }
  })
}
var deletenotificationDetails = function(req,res){
  // console.log('delete method');
  return notification.findByIdAndRemove(req.params.id, function (error, data) {
      if (error) return res.send({statusCode : 400});
      res.send({
          statusCode : 200,
          message : "deleted"
      });
      console.log('deleted');
  });
  }
  exports.addUserIntoSocket = addUserIntoSocket;
exports.createNotifications = createNotifications;
exports.getAllNotification = getAllNotification;
exports.notificationByEmail = notificationByEmail;
exports.updateNotificationDetails = updateNotificationDetails;
exports.deletenotificationDetails = deletenotificationDetails;