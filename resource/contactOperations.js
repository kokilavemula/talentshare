const contact = require('../model/contact');


const storeNewUserDetails = (req,res)=>{
    return contact.create(req.body).then(data=>{
        console.log('contact details stored successfully');
        return res.send({
            status : 201,
            message : data
        })
    })
    .catch(error=>{
        console.log('Error while storing user given details in contact form');
        return error;
    })
}

exports.storeNewUserDetails = storeNewUserDetails;