var projectDetails = require('../model/projectdetails');
var config = require('config');
const userIP = require('user-ip');
const audit = require('./auditOperations')
const createProjectDetails = (req, res)=>{
      console.log('req data',req.body);
        return projectDetails.create(req.body,function(error,data){
         
            if(error){
              const auditData = audit.userDetails(req,res);
              auditData.action = 'Create project Details';
              auditData.message = 'Error in Creating Project Details';
              audit.createAuditDetails(auditData,res);
              console.log('Error in creating project details');
              return res.send({
                status : 400,
                message : error
              });
            }
            else{
              const auditData = audit.userDetails(req,res);
              auditData.action = 'Çreate Project Details';
              auditData.message ='Project details created successfully';
              audit.createAuditDetails(auditData,res);
              console.log('Project Details Created Successfully');
                console.log('created data',data);
              return res.send({
                status : 200,
                message : data
              })
            }
        })           
}
const getAllProjectDetails = (req, res)=>{
  let ip = userIP(req);
  console.log('ip details',ip);
  let ipDetails = req.client.remoteAddress;
  console.log('req header details',req.headers);
  console.log('req client details',req.connection.remoteAddress);
  console.log('req ip details',ipDetails);
    return projectDetails.find(function(error,data){
        if(error){
          const auditData = audit.userDetails(req,res);
          auditData.action = 'get all project detils';
          auditData.message = 'Error in getting Project Details';
          audit.createAuditDetails(auditData,res);
          console.log('Error in getting all project details');
          return res.send({
            status : 400,
            message : 'bad request'
          });
        }
        else{
          const auditData = audit.userDetails(req,res);
          auditData.action = 'get all project details';
          auditData.message =' All Project details got successfully';
          audit.createAuditDetails(auditData,res);
          console.log('Project Details retrieved Successfully');
          console.log('getdata is',data);
          return res.send({
            status : 200,
            message : data
          })
        }
    })
}
const getOneProjectDetails = function(req,res){
  return projectDetails.findById(req.params.id,function(error,data){
      if(error){
        const auditData = audit.userDetails(req,res);
        auditData.action = 'get one project Details';
        auditData.message = 'Error in getting one  Project Details';
        audit.createAuditDetails(auditData,res);
        console.log('Error in getting one project details');
          return res.send({
              statusCode : 400,
              message : error
          });
      }
      else{
        const auditData = audit.userDetails(req,res);
        auditData.action = 'get one Project Details';
        auditData.message =' get one Project details successfully';
        audit.createAuditDetails(auditData,res);
        console.log('get one Project Details  Successfully');
         console.log('get One project details',data);
          res.send({
              statusCode : 200,
              message : data
          })
      }
  })
}
var updateProjectDetails = function(req,res){
  return projectDetails.findByIdAndUpdate(req.params.id,req.body,function(error,data){
      if(!error){
        const auditData = audit.userDetails(req,res);
        auditData.action = 'Update Project Details';
        auditData.message ='Project details updated successfully';
        audit.createAuditDetails(auditData,res);
        console.log('Project Details updated Successfully');
        console.log('data from put method : ',data);
        res.send({
            statusCode : 200,
            message : data
        });
      }
      else{
        const auditData = audit.userDetails(req,res);
        auditData.action = 'update project Details';
        auditData.message = 'Error in update  Project Details';
        audit.createAuditDetails(auditData,res);
        console.log('Error in update project details');
          res.send({
              statusCode : 500,
              error : error
          })
      }
  })
}
var deleteProjectDetails = function(req,res){
return projectDetails.findByIdAndRemove(req.params.id, function (error, data) {
    if (error) {
      const auditData = audit.userDetails(req,res);
      auditData.action = 'delete project Details';
      auditData.message = 'Error in delete  Project Details';
      audit.createAuditDetails(auditData,res);
      console.log('Error in delete project details');
      return res.send({statusCode : 400});
    }
    else{
      const auditData = audit.userDetails(req,res);
        auditData.action = 'delete Project Details';
        auditData.message ='Project details deleted successfully';
        audit.createAuditDetails(auditData,res);
        console.log('Project Details deleted  Successfully');
        res.send({
           statusCode : 200,
           message : "deleted"
        });
    }
    console.log('deleted');
  });
}
exports.createProjectDetails = createProjectDetails;
exports.getAllProjectDetails = getAllProjectDetails;
exports.getOneProjectDetails = getOneProjectDetails;
exports.updateProjectDetails = updateProjectDetails;
exports.deleteProjectDetails = deleteProjectDetails;
