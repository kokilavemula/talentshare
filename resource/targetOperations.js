var target = require('../model/target');
var config = require('config');
const createTarget = (req, res)=>{
    let userData = req.body;
    return target.create(userData,function(error,data){
        if(error){
          return res.send({
            status : 400,
            message : 'bad request'
          });
        }
        else{
            console.log('created target',data)
          return res.send({
            status : 200,
            message : data
          })
        }
    })
}
const getAllTargetDetails = (req, res)=>{
    return target.find(function(error,data){
        if(error){
          return res.send({
            status : 400,
            message : 'bad request'
          });
        }
        else{
            console.log('gettargetdata is',data);
          return res.send({
            status : 200,
            message : data
          })
        }
    })
}
const targetByEmail = (req, res)=>{
  var email = req.params.email;
  console.log('email is',email);
 return target.findOne({email}, function (err, users){
   // console.log(users._id);
   if(err){
     console.log(err);
   }
   else {
   console.log('target data:',users);
  return res.json(users);
   
   }
 }); 
};
var updateTargetDetails = function(req,res){
  return target.findByIdAndUpdate(req.params.id,req.body,function(error,data){
      if(!error){
          console.log('data from put method : ',data);
          res.send({
              statusCode : 200,
              message : data
          });
      }
      else{
          // console.log('error : ',error);
          res.send({
              statusCode : 500,
              error : error
          })
      }
  })
}
var deleteTargetDetails = function(req,res){
  // console.log('delete method');
  return target.findByIdAndRemove(req.params.id, function (error, data) {
      if (error) return res.send({statusCode : 400});
      res.send({
          statusCode : 200,
          message : "deleted"
      });
      console.log('deleted');
  });
  }
exports.createTarget = createTarget;
exports.getAllTargetDetails = getAllTargetDetails;
exports.targetByEmail = targetByEmail;
exports.updateTargetDetails = updateTargetDetails;
exports.deleteTargetDetails = deleteTargetDetails;