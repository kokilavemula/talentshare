var mocha = require('mocha');
var assert = require('assert');
var sinon = require('sinon');
var expect = require('chai').expert;
var db = require('../../db/index');
var q = require('q');
var config = require('config');
var Project = require('../../resource/projectdetailsOperations');
var idForOperation;
describe('ProjectDetails API Calls testing using unitTesting', function () {
    it('create new projectDetails', function (done) {
        let req = sinon.spy();
        let res = sinon.spy();
            res = { json : sinon.spy()}
            req = {
                body:{
                        "title" :"Ticket Management", 
                        "description":"handling tickets and give solution for that tickets",   
                        "requiredTools" :["angular,node,mongodb"] ,
                        "projectOverAllCost" : "30,000",
                        "projectFinalDelivery" : "3 months from starting of project",
                        "status" : "Applied"
                }
            }
            q.all([
                Project.createProjectDetails(req, res)
                
            ])
            .done(function(res){
                console.log('createProjectDetails',res);
                done();
            })
            
    })
})