var frisby = require('frisby');
var Joi = frisby.Joi;
var interpolate = require('interpolate');
var config = require('../../config/development')
var registerUrl = config.url.signup;
var idForOperation; 
const req = {
    headers:{
        "Accept":"application/Json"
    },
    body:{
        register:{
            "firstName":"kokila",
            "lastName":"vemula",
            "email":"kokila1@gmail.com",
            "password":"kokila"
        },
    }
}
frisby.globalSetup({
    request:{
        headers:req.headers
    }
})
var result = interpolate('{url}',{url:registerUrl},{ delimiter: '{}'});
// console.log('register url result',result);
describe('API testing for register using frisby', function () {
    it('register', function (done) {
      return frisby.post(`${result}`,req.body.register,{json : true})
        .expect('status', 200)
        .expect('header','content-type','application/json; charset=utf-8')
        .expect('jsonTypes','message',{
            firstName:Joi.string(),
            lastName: Joi.string(),
            email:Joi.string(),
            password:Joi.string(),
        })
        .then((registration)=> {
            idForOperation = registration._json.message._id;
            expect(registration).not.toBeNull()
            expect(registration._json.message.firstName).toBe(req.body.register.firstName);
            expect(registration._json.message.lastName).toBe(req.body.register.lastName);
            expect(registration._json.message.email).toBe(req.body.register.email);

        })
        .done(done); 
    })
    it('get All register details ', function (done) {
        return frisby.get(`${result}`)
        .expect('status',200)
        .expect('header','content-type','application/json; charset=utf-8')
        .then(getAllRegisterDetails=>{
            var data = getAllRegisterDetails._json.message;
            expect(getAllRegisterDetails).not.toBeNull();
            expect(data.length).toBeGreaterThan(0);
        })
        .done(done);
    })
})    