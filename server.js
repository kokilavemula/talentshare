var express = require('express'),
    cors = require("cors"),
    path = require('path');
    bodyParser = require('body-parser'),
    db = require('./db/index'),
    config = require('config'),
    signup = require('./routes/signup'),
    project = require('./routes/projectdetails'),
    questions = require('./routes/questionandanswer'),
    audit = require('./routes/audit');
    notification = require('./routes/notifications'),
    target = require('./routes/target'),
    contact = require('./routes/contact'), 
    mongoose = require('mongoose'),
    app = express();
    acl = require('./config/acl')
        db.connect(config.DB).then(()=>
        {
          console.log('mongodb connected');
        }) 

var swaggerDoc = require('./openapi.json');
var swaggerUi = require('swagger-ui-express');
const io = require('socket.io')(server);
app.use(bodyParser.json());
app.use(cors());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc));
app.use('/signup',signup);
app.use('/project',project);
app.use('/questions',questions);
app.use('/notification',notification);
app.use('/audit',audit);
app.use('/target',target);
app.use('/contact',contact);

// io.on('connection', (socket)=> {
//     console.log('user connected');
//     const socketData = socket.handshake.query.userId;
//     console.log('socket data is',socketData);
//     socket.emit('request', {message:'hi socket is connected'}); // emit an event to the socket
//     socket.on('disconnect', () => { 
//     console.log('user disconnected');
//      });

//   });
//   io.use((socket,next)=>{
//     const socketValue= socket.handshake.query.userId;
//     if(socketValue && socketValue == undefined){
//       return new Error('Authentication failed');
//     }else{
//       next();
//     }
//   })
io.use((socket,next)=>{
  let userId = socket.handshake.query.userId;
  if(userId == '' && undefined){
      console.log('user id : ',userId);
      socket.disconnect(true);
      socket.emit('request',socket.id);
      return new Error('Authentication Error');
  } else{
      next();
  }
})
io.on('connection',(socket)=>{
  logger.info('User Connected');
  const userId = socket.handshake.query.userId;
  notification.addUserIntoSocket(socket);
  socket.emit('request',{message : `Connected userId : ${userId}`});
  socket.on('disconnected',()=>{
      logger.info('User Disconnected');
  });
})

var port = process.env.PORT || 4000;
var server = app.listen(port, function(){
    console.log('Listening on port ' + port);
});

