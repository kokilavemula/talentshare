var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var questionandanswer = new Schema({
    actor : {
        objectType : String,
        displayName : String,
        id : String,
        email : String
    },
    object : {
        objectType : String
    },
    questionAndAnswers : [{
        question:String,
        answer:String
    }],
    status:String,
    projectId : String,
    appliedOn : {
        type : Date,
        default : Date.now
    }

});
module.exports = mongoose.model('questionandanswer',questionandanswer);
