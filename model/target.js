var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var target = new Schema({
        objectType : String,
        displayName : String,
        id : String,
      notifyId : String,
      status : String,
    viewedAt: {
        type : Date,
        default : Date.now
    }

});
module.exports = mongoose.model('target',target);
