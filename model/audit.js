var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var audit = new Schema({
    actor: {
        objectType : String,
        email : String,
        id : String
    },
    object : {
        objectType : String
    },
    browserDetails : {
        name : String,
        ipAddress : String
    },
    verb : String,
    message : String,
    action : String,
    Date : {
        type : Date,
        default : Date.now()
    }
    
});
module.exports = mongoose.model('audit',audit);

