var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var projectdetails = new Schema({
    title : {
        type : String
    },
    description: {
        type : String
    },
    requiredTools : {
        type : Array

    },
    actor:{
     email : String,
     name : String
    },
    projectOverAllCost : {
        type : String
    },
    projectFinalDelivery : {
        type : String
    },
    listOfQuestions : [{
        question : String
    }],
    status : String,
    createdOn : {
        type : Date,
        default : Date.now
      }
});
module.exports = mongoose.model('projectdetails',projectdetails);
