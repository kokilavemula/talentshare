var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var notification = new Schema({
    actor : {
        objectType : String,
        displayName : String,
        id : String
    },
    object : {
        objectType : String
    },
    verb: String,
    message: String,
    appliedOn : {
        type : Date,
        default : Date.now
    }

});
module.exports = mongoose.model('notification',notification);
