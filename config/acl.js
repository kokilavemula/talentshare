var express = require('express');
var mongoose = require('mongoose');  
var db = require("../db/index");
var acls = require("acl");
var config = require('./development')
var url = config.DB
var app = express();
var userId;
var newUserId;
var projectOperations = require('../resource/projectdetailsOperations');
var questions = require('../resource/questionandanswerOperations');
var userOperation = require('../resource/signupOperations');

var userIdFromToken = (req,res)=>{
db.connect(url).then(()=>{
  console.log("acl file .......",url);
      var acl = new acls(new acls.mongodbBackend(mongoose.connection.db,'acl_'));
      userId = req.params.id;
      console.log('user id is',userId);
  
})
}
const checkUsersAndRole = (req,res)=>{
    db.connect(url).then(()=>{
        var acl = new acls(new acls.mongodbBackend(mongoose.connection.db,'acl_'));
         newUserId = req.params.newUserId;
        acl.addUserRoles(newUserId,'SUBSCRIBER',(cb)=>{
            console.log('new user role added successfully',newUserId);
        })
    })
}
const checkProjectCreation = (req,res)=>{
    db.connect(url).then((db)=>{
    var acl=  new  acls(new acls.mongodbBackend(mongoose.connection.db,'acl_'))
        acl.allow(['ADMIN'],'/projectdetails','post',(err,cb)=>{
            console.log('successfully assigned resource and permission to particular role',cb);
        })
        acl.allowedPermissions(userId,'/projectdetails',(err,cb)=>{
            console.log('AllowedPermissions are : ',cb);
        })
        
        return acl.isAllowed(userId,'/projectdetails','post',(err,allow)=>{
            if(allow==true){
                console.log('allow : ',allow);
                projectOperations.createProjectDetails(req,res);
                return('user allowed successfully');
            } else{
                console.log('allow : ',allow);
                return res.send({
                    status: 403,
                    message: 'Error: Access Denied'
                })
            }
        })
    });
}
exports.userIdFromToken=userIdFromToken;
exports.checkUsersAndRole=checkUsersAndRole;
exports.checkProjectCreation=checkProjectCreation;