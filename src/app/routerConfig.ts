import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { EmployeeComponent } from './components/employee/employee.component';
import { AnswersComponent } from './components/answers/answers.component';
import { AdminComponent } from './components/admin/admin.component';
import { CreateProjectComponent } from './components/create-project/create-project.component';
import { ViewProjectComponent } from './components/edit-project/view-project.component';
import { ViewProjectsComponent } from './components/view-projects/view-projects.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { NotificationComponent } from './components/notification/notification.component';
import { ProjectListComponent } from './components/project-list/project-list.component';


export const appRoutes: Routes = [
    { path: 'home', 
    component: HomeComponent 
  },
  { path: 'register', 
    component: RegisterComponent 
  },
  { path: 'login', 
    component: LoginComponent 
  },
  { path: 'employee/:id', 
    component: EmployeeComponent 
  },
  { path: 'answers', 
    component: AnswersComponent 
  },
  { path: 'admin', 
    component: AdminComponent 
  },
  { path: 'create-project', 
    component: CreateProjectComponent 
  },
  { path: 'edit-project/:id', 
    component: ViewProjectComponent 
  },
  { path: 'view-projects/:id', 
    component: ViewProjectsComponent 
  },
  { path: 'user-details/:id', 
    component: UserDetailsComponent 
  },
  { path: 'users-list', 
    component: UsersListComponent 
  },
  { path: 'notification', 
    component: NotificationComponent 
  },
  { path: 'project-list', 
    component: ProjectListComponent 
  },
  {
        path : '',
        redirectTo : '/home',
        pathMatch : 'full'
      },
      {
        path : '**',
        redirectTo : '/home',
        pathMatch : 'full'
      },
];