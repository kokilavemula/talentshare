import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class InterceptorsService implements HttpInterceptor {
    constructor(){}
    intercept(req: HttpRequest<any> , next: HttpHandler): Observable <HttpEvent<any>>{
        let transformData = req;
        if (localStorage.length > 0){
            const token = localStorage.getItem('token');
            if(token) {
                transformData = req.clone({
                    setHeaders:{
                        'Authorization' : token
                    }
                });
            }
        }
        return next.handle(transformData);
    }
}