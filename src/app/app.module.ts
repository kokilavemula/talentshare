import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MyMaterialModule } from './material.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { NgxEditorModule } from 'ngx-editor';
import { RouterModule } from '@angular/router';
import { appRoutes } from './routerConfig';
import { UserService } from './user.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { EmployeeComponent } from './components/employee/employee.component';
import { LoginComponent } from './components/login/login.component';
import { AnswersComponent } from './components/answers/answers.component';
import { AdminComponent } from './components/admin/admin.component';
import { CreateProjectComponent } from './components/create-project/create-project.component';
import { ViewProjectComponent } from './components/edit-project/view-project.component';
import { ViewProjectsComponent } from './components/view-projects/view-projects.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { NotificationComponent } from './components/notification/notification.component';
import { ProjectListComponent } from './components/project-list/project-list.component';

import { HTTP_INTERCEPTORS } from '@angular/common/http';


import { MdePopoverModule } from '@material-extended/mde';
import { HttpModule } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';
import { InterceptorsService } from './interceptors.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegisterComponent,
    LoginComponent,
    EmployeeComponent,
    AnswersComponent,
    AdminComponent,
    CreateProjectComponent,
    ViewProjectComponent,
    ViewProjectsComponent,
    UserDetailsComponent,
    UsersListComponent,
    NotificationComponent,
    ProjectListComponent
  ],
  imports: [
    BrowserModule, RouterModule.forRoot(appRoutes),  MatFormFieldModule, FormsModule,FlexLayoutModule,
    MdePopoverModule,NgxEditorModule ,HttpModule,HttpClientModule,DataTablesModule ,
  MatInputModule,MyMaterialModule,ReactiveFormsModule , MyMaterialModule,BrowserAnimationsModule

    
  ], 
  providers: [UserService,
   {provide: HTTP_INTERCEPTORS, useClass: InterceptorsService, multi: true }],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  
})
export class AppModule { }



