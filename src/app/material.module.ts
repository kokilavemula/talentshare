import { NgModule } from  '@angular/core';
import {MatNativeDateModule,MatDatepickerModule,MatChipsModule,MatIconModule,MatBadgeModule,
MatButtonModule,MatCheckboxModule, MatToolbarModule, MatCardModule,MatFormFieldModule,
MatInputModule,MatRadioModule,MatListModule,MatMenuModule,  MatSelectModule, MatSidenavModule, MatTabsModule,MatGridListModule} from  '@angular/material';
// import {MatDatepickerModule} from  '@angular/material/datepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 //import {MatFormFieldModule, MatInputModule} from '@angular/material';


@NgModule({
imports: [MatNativeDateModule,MatDatepickerModule,MatGridListModule,MatChipsModule,MatFormFieldModule,
MatTabsModule,MatBadgeModule,MatSidenavModule,MatIconModule,MatButtonModule,MatCheckboxModule,
MatToolbarModule,FormsModule,MatCardModule,MatInputModule, MatSelectModule, MatListModule,MatRadioModule],


exports: [MatNativeDateModule,FormsModule,MatGridListModule,MatChipsModule,MatSidenavModule,MatDatepickerModule,MatTabsModule
,MatIconModule,MatMenuModule,MatButtonModule,MatCheckboxModule, MatBadgeModule, MatSelectModule,
MatToolbarModule, MatCardModule,MatFormFieldModule,MatInputModule, MatListModule,MatRadioModule],

})
 
export  class  MyMaterialModule { }
