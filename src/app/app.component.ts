import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {UserService} from './user.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'talent-share';
  flag : boolean;
  hideFlag : boolean;
  emailResponses;
  role;
  showAdmin: boolean;
  token;
  count;

   constructor(private userservice: UserService, private router : Router){
     this.hideFlag = false;
}
decodeToken(){
  if(localStorage.length == 0){
    return;
  }
  else{
    let token = localStorage.getItem('token');
    console.log('token is:',token);
    let splitToken = token.split('.');
    console.log('splited data:', splitToken);
    let idValue = JSON.parse(atob(splitToken[1]));
    console.log('id value:',idValue);
    return idValue;
  }
}
profile(){
  let userData = this.userservice.decodeToken();
  console.log('userdata is',userData);
   this.userservice.getSignUpByEmail(userData.email).subscribe(res => {
    console.log('res data is',res);
    this.emailResponses = res;
    console.log('emailResponses is',this.emailResponses);
    

  })
}
checkTokenValue(){
  if(localStorage.length >0){
    this.hideFlag = true;
  }
  else{
    this.hideFlag = false;
    this.router.navigate(['/login']);
  }
}

logout(){
  if(localStorage.length > 0 ){
    localStorage.clear();
    this.router.navigate(['/']);
    this.hideFlag = false;
  }
  else{
     this.router.navigate(['/login']);
     if(this.router.navigated){
      this.hideFlag = true;
    }
  }
}
notificationCount(){
  if(localStorage.length > 0){
    let userData = this.userservice.decodeToken();
    this.userservice.getAllTargetDetails().subscribe(res=>{
      this.count = res.message.length;
    })
  } else{
    this.count = 0;
    this.router.navigate(['/login']);
  }
}
notificationDetails(){
  this.notificationCount();
  this.router.navigate(['/notification']);
}
sendUserId(){
  if(localStorage.length > 0){
    let userData = this.userservice.decodeToken();
    this.userservice.userIdFromToken(userData.id).subscribe();
    this.userservice.getSignUpByEmail(userData.email).subscribe(res=>{
      this.role = res.message.role;
      if(this.role==='Admin'){
        this.showAdmin = true;
      } else{
        this.showAdmin = false;
      }
    })
  } else{
    swal.fire('Login','please login','info');
  }
}
hideAndShow(){
  if(localStorage.length >0){
    this.flag = true;
  } 
  
}
ngOnInit(){
  var idToken = this.userservice.decodeToken();
  console.log('íd token is',idToken);
  this.userservice.userIdFromToken(idToken.id).subscribe((res)=>{
    console.log('res from userIdFromToken : ',res);
  this.hideAndShow();
  this.profile();
  this.userservice.socketConnection();
})
this.sendUserId();
this.notificationCount();
}
}
