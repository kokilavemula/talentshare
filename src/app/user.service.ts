import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Http ,HttpModule} from '@angular/http';
import { catchError, map } from 'rxjs/operators';
import { Observable  } from 'rxjs';
import  io  from 'socket.io-client';

 const httpOptions = {
  headers : new HttpHeaders({ 'content-Type' : 'application/json'})
};

@Injectable()
 export class UserService {
  socket;
  // subject = new BehaviorSubject<number>(0);

  constructor(private http: HttpClient) {}
  private data(res: Response) {
    const body = res;
    return body || { };

  }
  
  signupDetails(data):Observable<any> {
    const url = 'http://localhost:4000/signup';
    return this.http.post(url,data);

  }
  loginDetails(data):Observable<any> {
    const url = 'http://localhost:4000/signup/login';
    return this.http.post(url,data);

  }
  getSignUpByEmail(email:string):Observable<any>{
    const url = `http://localhost:4000/signup/${email}`;
    console.log('url is',url);
  return this.http.get(url);

  }
  createProjectDetails(data):Observable<any> {
    const url = 'http://localhost:4000/project';
    return this.http.post(url,data);

  }
  getOneProjectDetails(id:string):Observable<any>{
    const url = `http://localhost:4000/project/${id}`;
    console.log('url is',url);
  return this.http.get(url);

  }
  getAllProjectDetails():Observable<any>{
    const url = 'http://localhost:4000/project';
    return this.http.get(url);
  
  }
  updateProjectDetails(id:string,data:string):Observable<any>{
    const url = `http://localhost:4000/project/${id}`;
    return this.http.put(url,data);
  }
  deleteprojectDetailsById(id:string):Observable<any> {
    const url = `http://localhost:4000/project/${id}`;
    return this.http.delete(url);
  
  }
  createQuestionAndAnswers(data):Observable<any> {
    const url = 'http://localhost:4000/questions';
    return this.http.post(url,data);

  }
  getAllQuestionAndAnswersDetails(id:string):Observable<any>{
    const url = `http://localhost:4000/questions/project/${id}`;
    console.log('project id is',url);
    return this.http.get(url);
    
  
  }
  getOneQuestionAndAnswersDetails(id:string):Observable<any>{
    const url = `http://localhost:4000/questions/${id}`;
    console.log('url is',url);
  return this.http.get(url);

  }
  //notification
  getOneNotificationDetails(id:string):Observable<any>{
    const url = `http://localhost:4000/notification/${id}`;
    console.log('url is',url);
  return this.http.get(url);

  }
  deleteNotificationDetailsById(id:string):Observable<any> {
    const url = `http://localhost:4000/notification/${id}`;
    return this.http.delete(url);
  }
  //target
  getOneTargetDetails(id:string):Observable<any>{
    const url = `http://localhost:4000/target/${id}`;
    console.log('url is',url);
  return this.http.get(url);

  }
  getAllTargetDetails():Observable<any>{
    const url = 'http://localhost:4000/target';
    return this.http.get(url);
  
  }
  updateTargetDetails(id:string,data:string):Observable<any>{
    const url = `http://localhost:4000/target/${id}`;
    return this.http.put(url,data);
  }
  deleteTargetDetailsById(id:string):Observable<any> {
    const url = `http://localhost:4000/target/${id}`;
    return this.http.delete(url);
  }
  contactDetails(data):Observable<any> {
    const url = 'http://localhost:4000/contact';
    return this.http.post(url,data);

  }
  
  decodeToken(){
    if(localStorage.length == 0){
      return;
    }
    else{
      let token = localStorage.getItem('token');
      console.log('token is:',token);
      let splitToken = token.split('.');
      console.log('splited data:', splitToken);
      let idValue = JSON.parse(atob(splitToken[1]));
      console.log('id value:',idValue);
      return idValue;
    }
  }
  // currentUser(id){
  //   console.log('id is ....................',id);
  //   return this.http.get(`${this.logininDetails}/loggedId/${id}`);

  // }
  userIdFromToken(id:string):Observable<any>{
    console.log('id from service file : ',id);
    const url = `http://localhost:4000/project/userId/${id}`;
    
    return this.http.get(url);
  }
  getNewUserId(newUserId:string):Observable<any>{
    console.log('new user id',newUserId);
    const url = `http://localhost:4000/project/new/userId/${newUserId}`;
    return this.http.get(url);
  }
  
  
socketConnection(){
  if(localStorage.length > 0){
    let userData = this.decodeToken();
    const userId = userData.id;
    let url = `http://localhost:4000/?userId=${userId}`;
    this.socket = io(url);
    this.socket.on('request', (res)=>{
      console.log('res from socketConnection emit function : ',res);
    })
    this.socket.on('new notification',(res)=>{
      console.log('res from new notification emit function : ',res);
    })
    this.socket.on('new target',(res)=>{
      console.log('res from new target emit function : ',res);
      
    })
    this.socket.on('notificationCount',(res)=>{
      console.log('res from notificationCount emit function : ',res);
      
    })
  }
}
}

  