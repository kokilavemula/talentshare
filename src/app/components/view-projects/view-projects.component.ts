import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user.service';
import {ActivatedRoute, Router} from "@angular/router";
@Component({
  selector: 'app-view-projects',
  templateUrl: './view-projects.component.html',
  styleUrls: ['./view-projects.component.css']
})
export class ViewProjectsComponent implements OnInit {
  flag : boolean;
  viewProjectData = [
  
  ];
  id;

  constructor(private api: UserService, private route : Router, private activated : ActivatedRoute) { }

  ngOnInit() {
    this.activated.params.subscribe(res => {
      this.id = res.id;
      console.log('id is', this.id);
      this.api.getOneProjectDetails(this.id).subscribe(res => {
        console.log('res is', res);
        this.viewProjectData = res.message;
      })
    })
  }

}
