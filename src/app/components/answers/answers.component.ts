import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user.service';
import {Router, ActivatedRoute} from "@angular/router";
import Swal from 'sweetalert2';
@Component({
  selector: 'app-answers',
  templateUrl: './answers.component.html',
  styleUrls: ['./answers.component.css']
})
export class AnswersComponent implements OnInit {
  answersAndQues;
  emailResponses;
  data = {
    ans1 : '',
    ans2: '',
    ans3: '',
    ans4: ''
  }
  projectId;
  id;
  createProjectData:any = [];
  temp:boolean;
decodeToken(){
  if(localStorage.length == 0){
    return;
  }
  else{
    let token = localStorage.getItem('token');
    console.log('token is:',token);
    let splitToken = token.split('.');
    console.log('splited data:', splitToken);
    let idValue = JSON.parse(atob(splitToken[1]));
    console.log('id value:',idValue);
    console.log('id :',idValue.id);
          return idValue;
  }
}



  constructor(private userservice: UserService, private route : Router, private routes: ActivatedRoute, ) { }
  submit(data){
    let filterData1 = data.ans1.replace(/<\/?[^>]+(>|$)/gi,'');
    let filterData2= data.ans2.replace(/<\/?[^>]+(>|$)/gi,'');
    let filterData3 = data.ans3.replace(/<\/?[^>]+(>|$)/gi,'');
    let filterData4= data.ans4.replace(/<\/?[^>]+(>|$)/gi,'');
    
 this.answersAndQues = {
  "actor" : {
    "objectType" : "person",
    "displayName" : "",
    "id" : "",
    "email" : ""
},
"object" : {
    "objectType" : "questions"
},
"questionAndAnswers" : [{ 
     "question": this.createProjectData.listOfQuestions[0].question,
          "answer": filterData1
        },
        {
          "question": this.createProjectData.listOfQuestions[1].question,
          "answer": filterData2
        },
        {
          "question": this.createProjectData.listOfQuestions[2].question,
          "answer": filterData3
        },
        {
          "question": this.createProjectData.listOfQuestions[3].question,
          "answer": filterData4
        }
],

"status":"Applied",
"projectId" : this.projectId
}


  }

  ngOnInit() {
  
if(localStorage.length > 0){
  this.routes.params.subscribe(res=>{
    this.projectId = res.id;
    this.userservice.getOneProjectDetails(this.id).subscribe(res=>{
      this.createProjectData = res.message;
      console.log("crwate Project Data is",this.createProjectData);
      this.temp = true;
      
    })
  });
  let userData = this.userservice.decodeToken();
this.userservice.getSignUpByEmail(userData.email).subscribe(res => {
console.log('res data is',res);
this.emailResponses = res;
this.answersAndQues.actor.displayName = this.emailResponses.firstName + ''+ this.emailResponses.lastName;
this.answersAndQues.actor.email = this.emailResponses.email;
 
  })
} else {
  this.route.navigate(['/login']);
}
  
  }
  editorConfig = {
    editable: true,
    minHeight: '5rem',
    "toolbar": [
      ["bold", "italic", "underline"],
      ["cut", "copy", "delete", "removeFormat", "undo", "redo"],
      ["link", "unlink", "image", "video"]
  ]
  }
}
