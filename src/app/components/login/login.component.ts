import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user.service';
import { Router } from '@angular/router';
import { AppComponent } from '../../app.component'; 
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  temp : boolean;
  role;
  loginData = {
    email : '',
    password : ''
  }
  token;
  constructor(private userservice: UserService, private router : Router,  private app: AppComponent) { }
  submit(loginData){
    console.log('login data from html',loginData);
    this.userservice.loginDetails(loginData).subscribe(res =>{
      this.token = res;
      console.log('login data from api : ',this.loginData);
       this.router.navigate(['/employee']);
       let key = 'token';
      localStorage.setItem(key,res.token);
      console.log('key name is : ',key);
      this.router.navigate(['/employee']);
    })
  }

  checkToken(){
  
    if ((localStorage.length == 0 || localStorage.length == undefined)) {
    // this.temp = false;
    this.router.navigate(["/login"]);
  }
  else{
    this.temp = false;       
     this.router.navigate(["/employee"]);
  }
}

checkUserRole(){
  this.token = this.userservice.decodeToken();
    this.userservice.getSignUpByEmail(this.token.email).subscribe(res=>{
      this.role = res.message.role;
      if(this.role==='Admin'){
        this.router.navigate(['/admin']);
      } else{
        this.router.navigate(['/project-list']);
      }
    })
}
  ngOnInit(){
    this.checkToken();
    this.checkUserRole();    
    this.app.sendUserId();
    this.app.notificationCount();
  }
  
}