import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user.service';
import {Router} from "@angular/router";
import Swal from 'sweetalert2';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { createDate } from 'ngx-bootstrap/chronos/create/date-from-array';
@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css']
})
export class CreateProjectComponent implements OnInit {
  angForm: FormGroup;
  responses;
  emailResponses;
  questions = {
    question1 : '',
    question2 : '',
    question3 : '',
    question4 : ''
  };
  createProjectData = {
  title :'',
  projectOverAllCost  :'',
  projectFinalDelivery :'',
  requiredTools:'',
  description:'',
  listOfQuestions :[
    {
      question : ""
    },
    {
      question : ""
    },
    {
      question : ""
    },
    {
      question : ""
    }
  ],
  status:'open',
  actor:{
    name: '',
    email:''
  }
  
}

show : boolean;
  constructor(private userservice: UserService, private route : Router) {this.show = false; }
  decodeToken(){
    if(localStorage.length == 0){
      return;
    }
    else{
      let token = localStorage.getItem('token');
      console.log('token is:',token);
      let splitToken = token.split('.');
      console.log('splited data:', splitToken);
      let idValue = JSON.parse(atob(splitToken[1]));
      console.log('id value:',idValue);
      console.log('id :',idValue.id);
            return idValue;
    }
}


  ngOnInit() {
    let userData = this.userservice.decodeToken();
     this.userservice.getSignUpByEmail(userData.email).subscribe(res => {
      console.log('res data is',res);
      this.emailResponses = res;
      this.createProjectData.actor.name = this.emailResponses.firstName + ''+ this.emailResponses.lastName;
      this.createProjectData.actor.email = this.emailResponses.email; 
     })
    
  }
  editorConfig = {
    editable: true,
    minHeight: '5rem',
    "toolbar": [
      ["bold", "italic", "underline"],
      ["cut", "copy", "delete", "removeFormat", "undo", "redo"],
      ["link", "unlink", "image", "video"]
  ]
  }
  submit(createProjectData){
    this.createProjectData.listOfQuestions = [
      {
        question : this.questions.question1
      },
      {
        question : this.questions.question2
      },
      {
        question : this.questions.question3
      },
      {
        question : this.questions.question4
      }
    ];
    
    // console.log('project data from html',createProjectData);
    this.userservice.createProjectDetails(createProjectData).subscribe(res =>{
      console.log('res from service file',res);
      this.responses = res;
      console.log('responses is',this.responses);
      if(this.responses.status==200){
       Swal.fire('Project','Project Created successfully','success');
     this.route.navigate(['/admin']);
       }
  else{
    Swal.fire('403','Access Denied','error');
    
     }
  })
    
  }
  
}
