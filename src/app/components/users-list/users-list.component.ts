import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user.service';
import {Router} from "@angular/router";
@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
  flag : boolean;
  taskData = [];
  id;
  emailResponses;
  constructor(private api : UserService, private route : Router ) { this.flag = false; }
   dtOptions:DataTables.Settings = {}
  getAllProjectDetails() {
    this.api.getAllQuestionAndAnswersDetails(this.id).subscribe(res=>{
      console.log('res from projectList : ',res);
        this.taskData = res.message; 
        console.log('....................data......................',this.taskData);
       this.flag = true;
    })
  }
  getAllProject(taskData){
    let userData = this.api.decodeToken();
     this.api.getSignUpByEmail(userData.email).subscribe(res => {
      console.log('res data is',res);
      this.emailResponses = res;
      taskData.actor.name = this.emailResponses.firstName + ''+ this.emailResponses.lastName;
      taskData.actor.email = this.emailResponses.email; 

    })
  }

  ngOnInit() {
    this.getAllProjectDetails();
  }

}
