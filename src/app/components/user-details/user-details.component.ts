import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user.service';
import {ActivatedRoute, Router} from "@angular/router";
import Swal from 'sweetalert2'
@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  id;
employeeDetailsData={
  appliedOn:'',
  actor:{
    displayName:'',
    email:''
  },
  questionAndAnswers : [{
    question:'',
    answer:''
}],
}
  constructor(private api : UserService, private route : Router, private activated :ActivatedRoute) { }

  ngOnInit() {
    this.activated.params.subscribe(res => {
      this.id = res.id;
      console.log('id is', this.id);
      this.api.getOneQuestionAndAnswersDetails(this.id).subscribe(res => {
        console.log('res is', JSON.stringify(res.message));
        this.employeeDetailsData = res.message;
      
      })
    })
  }
  submit(employeeDetailsData){
    this.route.navigate(['/users-list']);
    Swal.fire("you are selected to this project");
  }
  

  reject(){
    this.route.navigate(['/users-list']);
    Swal.fire("you are reject to this project");
  }
  

}
