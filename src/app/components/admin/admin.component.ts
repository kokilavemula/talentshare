import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user.service';
import {Router} from "@angular/router";
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  flag : boolean;
  taskData = [];
  constructor(private api : UserService, private route : Router ) { this.flag = false; }
   dtOptions:DataTables.Settings = {}
  
  getAllProjectDetails() {
    this.api.getAllProjectDetails().subscribe(res=>{
      console.log('res from projectList : ',res);
        this.taskData = res.message; 
        console.log('data is',this.taskData);
       this.flag = true;
    })
  }
 
  deleteProjectDetails(id) {
    console.log('id is',id);
    this.api.deleteprojectDetailsById(id).subscribe(res=>{
      console.log('res from delete : ',res);
      this.getAllProjectDetails();
    })
  }
  
  ngOnInit() {
    this.getAllProjectDetails();
  }

}
