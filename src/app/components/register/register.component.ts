import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
 
  registerData = {
    firstName : '',
    lastName : '',
    email : '',
    password :''
  }
  
  constructor(private userservice: UserService, private router : Router) { }
  registerDetails(registerData){
    this.userservice.signupDetails(registerData).subscribe(res =>{
      console.log('Registered successfully');
      this.registerData = res;
      console.log(this.registerData);
      this.router.navigate(["/login"]);
      this.userservice.getNewUserId(res.message._id).subscribe(res=>{
        console.log('NewUserId send to backend successfully');
      })
    })
  }
  ngOnInit() {
  }

}
