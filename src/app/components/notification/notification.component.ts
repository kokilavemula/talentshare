import { Component, OnInit } from '@angular/core';
import io from 'socket.io-client';
import { UserService } from '../../user.service';
import {ActivatedRoute, Router} from "@angular/router";
import Swal from 'sweetalert2';
import { Subscriber } from 'rxjs';
// import { AppComponent } from 'src/app/app.component'

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
id;
emailResponses;
  constructor(private api: UserService ,private route : Router, private activated : ActivatedRoute) { }
notifications = [];
emptyFlag: boolean;
loggedUser;
p:Number;

sizes = [
  { value:5,viewValue:5},
  { value:10,viewValue:10},
  { value:15,viewValue:15},
  { value:20,viewValue:20}
];
viewNotificationDetails(notifyData,_id){
  Swal.fire({
    title : "more info",
    type : "info",
    html :`<p> Notification from : ${notifyData.displayName}<br>message:${notifyData.message}</p>`

  });
  this.api.getOneTargetDetails(this.id).subscribe(res=>{
    let data = res.message;
    data.status = 'read';
    this.api.updateTargetDetails(this.id,data).subscribe(resData=>{
    //  this.app.notification
    })
  })
}
notificationList(){
  if(localStorage.length>0){
  let userData =this.api.decodeToken;
  this.api.getAllTargetDetails().subscribe(res=>{
    if(res.message.length>0){
      this.notifications = res.message;

    }else{
      this.emptyFlag=true;
    }
  })

  }
}
deleteNotification(id){
  this.api.deleteTargetDetailsById(id).subscribe(res=>{
    this.notificationList();
  })
}
decodeToken(){
  if(localStorage.length == 0){
    return;
  }
  else{
    let token = localStorage.getItem('token');
    console.log('token is:',token);
    let splitToken = token.split('.');
    console.log('splited data:', splitToken);
    let idValue = JSON.parse(atob(splitToken[1]));
    console.log('id value:',idValue);
    console.log('id :',idValue.id);
          return idValue;
  }
}
  ngOnInit() {
    this.notificationList();
    let userData = this.api.decodeToken();
    console.log('user   ....... data is',userData);
     this.api.getSignUpByEmail(userData.email).subscribe(res => {
      console.log('res data is',res);
    
    })
  }

}
