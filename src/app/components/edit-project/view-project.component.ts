import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-view-project',
  templateUrl: './view-project.component.html',
  styleUrls: ['./view-project.component.css']
})
export class ViewProjectComponent implements OnInit {
  ProjectData = {
    title :'',
    projectOverAllCost :'',
    projectFinalDelivery :'',
    requiredTools:'',
    status:'',
    description:''
  }
  id;
  constructor(private api: UserService, private route : Router, private activated : ActivatedRoute) { }
  sizes = [
    {value : 'close',viewValue: 'close'},
    {value : 'open',viewValue: 'open'}
    
  ];
  ngOnInit() {
    this.activated.params.subscribe(res => {
      this.id = res.id;
      console.log('id is', this.id);
      this.api.getOneProjectDetails(this.id).subscribe(res => {
        console.log('res is',res);
         this.ProjectData = res.message;
        //  console.log("status value",this.statusValues);
      })
    })
  }
  updateProjectDetails(id,data) {
    console.log('project details data from html', id);
    this.api.updateProjectDetails(id,data).subscribe(res => {
    console.log('project details data from api : ',id);
    console.log('updated project details : ',res);
    Swal.fire("project updated");
    this.route.navigate(["/admin"]);
  });
  }
  editorConfig = {
    editable: true,
    minHeight: '5rem',
    "toolbar": [
      ["bold", "italic", "underline"],
      ["cut", "copy", "delete", "removeFormat", "undo", "redo"],
      ["link", "unlink", "image", "video"]
  ]
  }
  

}
