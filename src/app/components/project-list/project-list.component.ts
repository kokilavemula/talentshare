import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user.service';
import {Router} from "@angular/router";
@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {
  flag : boolean;
  projectListData = [];
  constructor(private api : UserService, private route : Router ) { this.flag = false; }
   dtOptions:DataTables.Settings = {}
   getAllProjectDetails() {
    this.api.getAllProjectDetails().subscribe(res=>{
      console.log('res from projectList : ',res);
        this.projectListData = res.message; 
        console.log('data is',this.projectListData);
       this.flag = true;
    })
  }
  ngOnInit() {
    this.getAllProjectDetails();
  }

}

