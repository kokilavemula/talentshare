import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import { UserService } from '../../user.service';
import {ActivatedRoute, Router} from "@angular/router";
import swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  data = {
    name: '',
    email: '',
    subject: '',
    message: ''
  }

  searchCtrl = new FormControl('');
  listOfProjects = [];
  matchedValue;

  constructor(private userservice: UserService, private router: Router) {
    this.searchCtrl.valueChanges.subscribe(this.search.bind(this));
   }
  ngOnInit() {
  }

  search(value){
    console.log('value : ',value);
    this.userservice.getAllProjectDetails().subscribe(res=>{
      this.listOfProjects = res.message;
      console.log('this.list : ',this.listOfProjects);
      for(let projectData of this.listOfProjects){
        this.matchedValue = this.listOfProjects.filter((projectData)=>{
          return projectData.title == value;
        });
      }
      if(this.matchedValue.length > 0){
        this.router.navigate([`/employee/${this.matchedValue[0]._id}`]);
      }
    });
  }

  submit(data){
    this.userservice.contactDetails(data).subscribe(res=>{
      console.log('res from submit method : ',res);
      this.data = {
        name : '',
        email : '',
        subject : '',
        message : ''
      }
    })
  }
}


