const express = require('express');
const router = express.Router();
var projectNotification = require('../resource/notificationOperations');
router.post('/',projectNotification.createNotifications);
router.get('/',projectNotification.getAllNotification);
router.get('/:email',projectNotification.notificationByEmail);
router.put('/:id',projectNotification.updateNotificationDetails);
router.delete('/:id',projectNotification.deletenotificationDetails);
module.exports = router;