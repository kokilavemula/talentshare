const express = require('express');
const router = express.Router();
var projectTarget = require('../resource/targetOperations');
router.post('/',projectTarget.createTarget);
router.get('/',projectTarget.getAllTargetDetails);
router.get('/:email',projectTarget.targetByEmail);
router.put('/:id',projectTarget.updateTargetDetails);
router.delete('/:id',projectTarget.deleteTargetDetails);
module.exports = router;